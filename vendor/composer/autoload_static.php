<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit11b591cbfa418610b73f34b5dac6698c
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'Lumio\\IntegrationAPI\\' => 21,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Lumio\\IntegrationAPI\\' => 
        array (
            0 => __DIR__ . '/..' . '/lumioanalytics/lia-php-client/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit11b591cbfa418610b73f34b5dac6698c::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit11b591cbfa418610b73f34b5dac6698c::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
